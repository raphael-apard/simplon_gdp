<?php require_once '../includes/process.php'; ?>
<?php include '../includes/header.php';?>

<div class="container_add_dev">
        <div class="dev">
            <form  class="form_add_dev" method="POST">
                <input type="hidden" name="id_dev" value="<?php ;//echo $row['Id']; ?>">
                <label for="post_lastname_dev">Nom de famille</label>
                <input type="text" id="post_lastname_dev" name="post_lastname_dev" value="<?php ;//echo $project; ?>">
                <label for="post_firstname_dev">Prénom</label>
                <input type="text" id="post_firstname_dev" name="post_firstname_dev" value="<?php ;//echo $project; ?>">
                
                <div class="filters">
                <select name="post_dev_level">
                    <option hidden>Niveau</option>


                    
                        <option hidden>Liste des personnes</option>
                        <?php
                            while ($lev = $query_niveaux->fetch_assoc()): ?>
                              
                              <option id="post_dev_level"  value="<?php echo $lev['Id']; ?>">
                        <?php 
                        echo $lev['level'];?></option>
                        <?php endwhile; ?>
                    
                    
                </select> 
                </div>
                
                <?php
                //changer le bouton "enregistrer" en "update" quand le bouton modifier a été cliqué
                if ($update_dev == true):
                ?>

                <button type="submit" name="update_dev">Modifier</button>
                <?php else: ?>
        	        <button type="submit" name="enregistrer_dev">Enregistrer</button>
                <?php endif; ?>

            </form>  
            
            
        </div>

</div>
<a href="../index.php"><< Retour</a>

    <?php include '../includes/footer.php';?>