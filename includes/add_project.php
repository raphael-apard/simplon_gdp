<?php require_once '../includes/process.php'; ?>
<?php include '../includes/header.php';?>

<div class="container_add_projet">
        <div class="container_add_project">
            <form  class="form_add_project" method="POST">
                <input type="hidden" name="id_projet" value="<?php echo $row['Id']; ?>">
                <label for="post_name_project">Nom du projet</label>
                <input type="text" id="post_name_project" name="post_name_project" value="<?php echo $project; ?>">
        	    <label for="post_description_project">Description</label>
                <textarea id="post_description_project" name="post_description_project" rows="7" ><?php echo $description_project; ?></textarea>
                
                <?php
                //changer le bouton "enregistrer" en "update" quand le bouton modifier a été cliqué
                if ($update == true):
                ?>

                <button type="submit" name="update">Modifier</button>
                <?php else: ?>
        	        <button type="submit" name="enregistrer">Enregistrer</button>
                <?php endif; ?>

            </form>  
        </div>

        
    </div>
    <a class="return" href="../index.php"><< Retour</a>

    <?php include '../includes/footer.php';?>