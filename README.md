# Simplon_GDP


## Instalation

Télécharger le dossier

Importer la base de données dans phpMyAdmin.

## Connexion à la base de données

renommer le fichier connexion_exemple.php en connexion.php et indiquez votre nom de serveur, nom de base de donnée, nom d'utilisateur et mot de passe.

```php
<?php

$hn = 'nomServer';
$db = 'nomDeBDD';
$un = 'userName';
$pw = 'motDePasse';

?>
```
## Lancer le serveur

Ouvrez un terminal

Mettez-vous à la racine de votre projet

Lancez votre serveur par la commande : php -S localhost:8000